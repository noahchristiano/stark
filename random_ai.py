#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from random import randint
import sys
import time
import re

def warning(*objs):
    print('', *objs, file=sys.stderr)

num_players = 2 # number of players in the game

def get_player():
	player = 0;
	line = ''
	compare = 'You are: Player ' # ends when this string is seen
	for s in iter(raw_input, ''):
		if compare in s:
			player = s[16:]
			return int(player)
	return 0 # error

def process_rank(card):
	return card[:-3]

def process_suit(card):
	suit = card[(len(card)-3):]

	if suit == '♠':
		suit = 's'
	elif suit == '♣':
		suit = 'c'
	elif suit == '♥':
		suit = 'h'
	elif suit == '♦':
		suit = 'd'

	return suit

def process_card(card):
	card = process_rank(card) + process_suit(card)
	return card

def process_cards(cards):
	processed_cards = []
	for card in cards:
		processed_card = process_card(card)
		processed_cards.append(processed_card)
	return processed_cards

def get_cards():
	print('status:')
	line = raw_input()
	if line != None and len(line) > 1:
		search = re.search(r"[^a-zA-Z](] Last:)[^a-zA-Z]", line)
		if search != None:
			end = len(line) - search.start(1)
			line = line[8:-end] # extract the string of cards
			cards = line.split(',')
			cards = process_cards(cards)
			return cards
	return get_cards()

def play_random_card(cards):
	rand = randint(0, len(cards)-1)
	return cards[rand]

def get_last(turn):
	if turn-1 == 0:
		return num_players
	return turn-1

def get_next(turn):
	if turn+1 > num_players:
		return 1
	return turn+1

def update(line, player):
	penalties = get_penalties(line, player)
	warning("Penalties: " + penalties)
	phrase = get_phrase(line)
	warning("Phrase: " + phrase)
	state = get_state()
	last_card = get_last_card()
	warning("Last card: " + last_card)

def next_turn(current, last, player):
	line = raw_input()
	#update(line, player)
	if 'Player ' + str(current) + ' places' in line:
		return True;
	if current != last and 'Player ' + str(current) + ' is penalized ' in line:
		return True;
	return False;

#  -return which penalty/effect the player incurred (if any), and any additional information about it:
#   >continue/allow
#   >block
#   >reverse
#   >skipTo & {int between 0 and numPlayer (inclusive, exclusive)}
#   >mustSay
# -this can be just a string + additional relevant information, or some integer code corresponding to each penalty. The one I'm currently  using is 0=continue/allow, block=1, reverse=2, skipTo={3 to number of players}, mustSay = {number of players}
def get_penalties(line, player):
	if len(line) > 28 and 'Player ' + str(player) + ' is penalized ' in line:
		return line[27:]
	return ''

#  -if the active player said some phrase during their move, store/return what they said as a string
def get_phrase(line):
	if len(line) > 16 and 'says:' in line:
		return line[15:]
	return ''

def get_last_card():
	print('status:')
	line = raw_input()
	card = ''
	if line != None and len(line) > 1:
		search = re.search(r"[^a-zA-Z](Last:)[^a-zA-Z]", line)
		if search != None:
			end = len(line) - search.start(1)
			line = line[(len(line)-6):] # extract the string of cards
			cards = process_card(line)
	return card

 # -suit of lastCard
 #   >0 = hearts, 1 = diamonds, 2 = spades, 3 = clubs
 # -rank of lastCard
 #   >integer from 0 to 13 (or 1 to 14, but specify if so)
 # -number of cards in active player's hand
def get_state():
	pass

def play_round():
	round_complete = False
	player = 0
	turn = 1
	last = 0
	penalties = ''
	warnings = ''
	phrase = ''
	state = ''
	get_last_card = ''

	player = get_player()
	warning("Random AI is player " + str(player))
	while(not round_complete):
		if turn == player:
			time.sleep(4)
			cards = get_cards()
			warning("Hand " + str(cards))
			rand_card = play_random_card(cards)
			print('place: ' + rand_card)
			warning("place: " + rand_card)
		if next_turn(turn, last, player):
			turn = get_next(turn)
		else:
			last = turn

def main():
	while(True):
		play_round()

if __name__ == '__main__':
	main()